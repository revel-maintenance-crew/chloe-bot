import os
from hata import Client, start_clients, stop_clients, events, sleep
from hata.events import ContentParser

from core import get_card_level, update_card_levels, get_SolarSystems, get_planets, AddCrew

with open('token.txt') as f:
    TOKEN = f.read()

chloe = Client(TOKEN)

on_command = chloe.events(events.CommandProcesser('chloe ')).shortcut

@chloe.events
class once_on_ready(object):
    __slots__ = ('called', )
    __event_name__ = 'ready'

    def __init__(self):
        self.called = False

    async def __call__(self, client):
        if self.called:
            return
            self.called = True
        await update_card_levels()
        print(f'{chloe:f} has been started')

@on_command
async def help(client, message):
    help_msg = ''
    for command in chloe.events.message_create.commands:
        help_msg = help_msg + command + '\n'
    await client.message_create(message.channel, f'Commands are:```{help_msg}```')

@on_command
async def SolarSystems(client, message):
    await update_card_levels()
    CardLevel = get_card_level(message.author.id)
    if CardLevel <= 0 or CardLevel >= 16:
        await client.message_create(message.channel, "Sorry, you are not registered, so you are not able to use this command, if you want to get registered, please ask Proxy")
    await client.message_create(message.channel, f"Here is a list of all of the solar systems```{get_SolarSystems()}```")

@on_command
@ContentParser('str', 'rest')
async def PlanetsInSystem(client, message, SolarSystem, idrc):
    await update_card_levels()
    CardLevel = get_card_level(message.author.id)
    if CardLevel <= 0 or CardLevel >= 16:
        await client.message_create(message.channel, "Sorry, you are not registered, so you are not able to use this command, if you want to get registered, please ask Proxy")
    try:
        Planets = get_planets(SolarSystem)
    except FileNotFoundError:
        await client.message_create(message.channel, f"`{SolarSystem}` is not a valid system, please enter a valid system")
        return
    if Planets == 'Empty':
        await client.message_create(message.channel, f"{SolarSystem} has no recorded planets yet")
        return
    await client.message_create(message.channel, f"Here is a list of all of the planets in {SolarSystem}:```{Planets}```")

@on_command
async def CardCheck(client, message):
    await update_card_levels()
    CardLevel = get_card_level(message.author.id)
    if CardLevel <= 0 or CardLevel >= 16:
        await client.message_create(message.channel, "Your card is invalid, please contact Proxy to resolve this, this might be because you are unregistered or because of corruption...")
        return
    await client.message_create(message.channel, f'Your Card has access level {str(CardLevel)}')

@on_command(case='lib-update')
async def lib_updater(client, message):
    if get_card_level(message.author.id) < 13:
        await client.message_create(message.channel,"Access denied, You must have card permission 13 or higher to update the system's core")
        return
    await client.message_create(message.channel,"Updating the system's core now...")
    client.loop.create_task(os.system("python3 -m pip install -U https://github.com/HuyaneMatsu/hata/archive/master.zip"))
    await client.message_create(message.channel, 'Updated the system successfully, please restart the system for changes to take affect')

@on_command
@ContentParser('int', 'int', 'rest')
async def AddMember(client, message, ID, LevelOfCard, idrc):
    await update_card_levels()
    CardLevel = get_card_level(message.author.id)
    if CardLevel < 13:
        await client.message_create(message.channel, "Access denied, you need to have a card level of 13 or higher")
        return
    await client.message_create(message.channel, f"Adding <@{str(ID)}> to the Crew list with CardLevel {LevelOfCard}")
    AddCrew(ID, LevelOfCard)
    await client.message_create(message.channel, f"Done!")

start_clients()
