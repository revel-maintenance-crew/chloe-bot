import pickle
import os
from subprocess import getoutput

from hata import KOKORO, Lock

CARD_LEVELS = {}
CARD_LEVELS_LOCK = Lock(KOKORO)

async def update_card_levels():
    async with CARD_LEVELS_LOCK:
        data = await KOKORO.run_in_executor(_get_pickle_card_levels)
        
        old_ids = set(CARD_LEVELS)
        for id_, level in data.items():
            id_ = int(id_)
            try:
                old_ids.remove(id_)
            except KeyError:
                pass

            CARD_LEVELS[id_]=level

        for id_ in old_ids:
            del CARD_LEVELS[id_]

def _get_pickle_card_levels():
    with open('cardlevels.pkl','rb') as file:
        return pickle.load(file)

def get_card_level(id_):
    return CARD_LEVELS.get(id_,0)

KOKORO.create_task_threadsafe(update_card_levels()).syncwrap().wait()

def get_SolarSystems():
    X = ''
    temp = getoutput('ls ./SolarSystems/').split('\n')
    for i in temp:
        X = f"{X}{i[:-12]}\n"
    return X

def get_planets(SolarSystem):
    Planets = ''
    with open(f'./SolarSystems/{SolarSystem}.solarsystem', 'rb') as file:
        temp = pickle.load(file)
    if temp == []:
        return 'No data provided for that system'
    for Planet in temp:
        Planets = f"{Planets}{Planet}\n"
    return Planets

def AddCrew(ID, CardLevel):
    Crew = _get_json_card_levels()
    Crew[str(ID)]=int(CardLevel)
    with open('cardlevels.pkl',"w+") as f:
        pickle.dump(Crew, f)
    return
