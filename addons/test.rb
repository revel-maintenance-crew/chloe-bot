require 'discordrb'
require 'confrb'

token = File.read('token.txt')

chloe = Discordrb::Commands::CommandBot.new token: token, prefix: 'chloe ' # Make a bot constructor with the right settings

chloe.command :help do |event|
  event.respond ""
end

chloe.command :test do |event|
  event.respond "This is a test with Ruby"
end

chloe.command :feedback do |event, *content|
  msg = event.respond "#{event.author.name} suggested: `#{content.join ' '}`"
  msg.react '✅'
  msg.react '❌'
  msg.react '❓'
end

chloe.run
